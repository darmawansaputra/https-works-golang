package main

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"encoding/gob"
	"fmt"
	"log"
	"net"
)

type Certificate struct {
	Version   string
	PublicKey rsa.PublicKey
}

type Packet struct {
	Type int
	Data []byte
}

const (
	TCertificate = iota
	TSecretKey   = iota
	TOK          = iota
	TData        = iota
)

func main() {
	packet := &Packet{}
	certificate := &Certificate{}
	var buff bytes.Buffer
	encBuff := gob.NewEncoder(&buff)
	decBuff := gob.NewDecoder(&buff)
	server := "localhost:8000"

	fmt.Println("+ Starting client...")
	fmt.Println("+ Attempt connect to", server)
	conn, err := net.Dial("tcp", server)

	if err != nil {
		log.Fatal("+ Failed connect to the server", err)
	}

	fmt.Println("+ Client successfully connected to the server...")

	decConn := gob.NewDecoder(conn)
	decConn.Decode(packet)

	if packet.Type == TCertificate {
		fmt.Println("+ Client receive server's certificate...")

		buff.Write(packet.Data)
		decBuff.Decode(certificate)

		fmt.Printf("  - Server's Public Key: %v\n", certificate.PublicKey)
	}

	// Generate secret key
	secretKey := GenerateSecretKey(16)
	fmt.Println("+ Client generate secret key:", base64.URLEncoding.EncodeToString(secretKey))

	// Fill packet data
	packet = &Packet{TSecretKey, secretKey}

	// Encryption secret key packet data with server's public key
	label := []byte("")
	buff.Reset()
	encBuff.Encode(packet)

	fmt.Println("+ Client encrypt secret key packet with server's public key...")
	cipherText, err := rsa.EncryptOAEP(sha256.New(), rand.Reader, &certificate.PublicKey, buff.Bytes(), label)

	fmt.Println("  - Result CipherText bytes:", cipherText)

	// Send encryption data to server
	fmt.Println("+ Client send secret key CipherText to server...")
	encConn := gob.NewEncoder(conn)
	encConn.Encode(cipherText)

	// Get packet from server
	var data string
	decConn.Decode(&data)

	fmt.Println("+ Client receive AES CipherText:", data)

	// AES Decode
	text, _ := decrypt(secretKey, data)
	fmt.Println("+ Client decrypt CipherText with secret key...")

	buff.Reset()
	buff.Write([]byte(text))

	packet = &Packet{}
	decBuff.Decode(packet)

	fmt.Println("  - Server says:", string(packet.Data))

	conn.Close()
}

func GenerateSecretKey(n int) []byte {
	key := make([]byte, n)
	_, err := rand.Read(key)

	if err != nil {
		return nil
	}

	return key
}

package main

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"encoding/gob"
	"fmt"
	"net"
)

type Certificate struct {
	Version   string
	PublicKey rsa.PublicKey
}

type Packet struct {
	Type int
	Data []byte
}

const (
	TCertificate = iota
	TSecretKey   = iota
	TOK          = iota
	TData        = iota
)

func main() {
	var secretKey []byte
	var buff bytes.Buffer
	encBuff := gob.NewEncoder(&buff)
	decBuff := gob.NewDecoder(&buff)

	fmt.Println("+ Server starting service...")

	ln, err := net.Listen("tcp", ":8000")
	defer ln.Close()

	if err != nil {
		// Listen() handle
	}

	fmt.Println("+ Server running...")
	fmt.Println("+ Server waiting a client...")

	conn, err := ln.Accept()
	defer conn.Close()

	if err != nil {
		// Accept() handle
	}

	fmt.Println("+ Server got a client...")

	// Generate private and public key with rsa algorithm
	serverPrivateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	certificate := Certificate{"v3", serverPrivateKey.PublicKey}

	encBuff.Encode(certificate)
	packet := &Packet{TCertificate, buff.Bytes()}

	encConn := gob.NewEncoder(conn)
	encConn.Encode(packet)

	fmt.Println("+ Server send the certificate...")

	// Get secret key cipher text
	var data []byte
	decConn := gob.NewDecoder(conn)
	decConn.Decode(&data)

	fmt.Println("+ Server receive secret key's client RSA CipherText bytes:", data)

	// Decrypt client data, with server's private key
	label := []byte("")
	fmt.Println("+ Server decrypt secret key cipher with private key...")
	plainText, err := rsa.DecryptOAEP(sha256.New(), rand.Reader, serverPrivateKey, data, label)

	buff.Reset()
	buff.Write(plainText)
	packet = &Packet{}
	decBuff.Decode(packet)

	fmt.Println("  - Result decrypt:", packet)

	if packet.Type == TSecretKey {
		secretKey = packet.Data
		fmt.Println("+ Server got client's secret key:", base64.URLEncoding.EncodeToString(secretKey))
	}

	/*isFirst := true
	for {

		isFirst = false
	}*/
	// Server send OK data encrypt with symmetric client key
	packet = &Packet{TData, []byte("OK Client!")}
	buff.Reset()
	encBuff.Encode(packet)

	aesCipher, _ := encrypt(secretKey, buff.Bytes())

	fmt.Println("+ Server send data 'OK Client!' symmetric encrypt packet to client...")
	fmt.Println("  - AES CipherText:", aesCipher)

	encConn.Encode(aesCipher)
}
